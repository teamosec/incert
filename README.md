#inCert#

inCert is a tool that uses the TStore class to add certificates to the trusted store database.

The TrustedStore database is an sqlite database containing apples trusted ssl certificates.


##TStore##

Is the class that responsible for parsing the special certificates format created at homeside (made by the certXML script) and inserting the relevant info into the TrustedStore database in Apples format.

##certXML##

Is a script configured for our use to decode certificates binary info needed in the TrustedStore database, encoding the info in base64 and inserting it to xml file.

The output file is the file that needs to be processed by inCert at the attacked iPhone.

--------------------------------------------

##Instructions##

1. Format the certificates at home 
     **certXML.py -a <PEM Format certificate>**

2. send output and inCert binary to the attacked iPhone 

3. run
     **inCert <Special format certificate>**

###Tests###

* **Tested on all iOS devices versions 7.1.2 - 8.1**
* *Optional to add encryption decryption methods.* 
* *No Dependencies.*