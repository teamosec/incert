GO_EASY_ON_ME = 1
include theos/makefiles/common.mk

TOOL_NAME = inCert
inCert_FILES = main.m SQLiteManager.m TStore.m XMLReader.m
inCert_LDFLAGS = -lsqlite3

include $(THEOS_MAKE_PATH)/tool.mk
