#include <Foundation/Foundation.h>
#include "TStore.h"
#import <sqlite3.h>

/*================================
	Testing And Debugging Only
================================*/

int cleanAndInsert(char *cert);

//Delete old TrustStore db values and add new ones
int main(int argc, char **argv, char **envp) {

	@autoreleasepool {
		return cleanAndInsert(argv[1]);
	}

}

int cleanAndInsert(char *cert) {

	NSFileManager *fileManager = [NSFileManager defaultManager];
	NSString *certPath = [NSString stringWithFormat:@"%s", cert];

	if ([fileManager fileExistsAtPath:certPath]){ 

		TStore *tstore = [[TStore alloc] init];
       	 	NSArray *result = [tstore runQuery:@"SELECT * from tsettings"];
        	NSLog(@"deleting trustStore db values: \n\n %@", result);


        	[tstore runQuery:@"DELETE from tsettings"];

        	NSLog(@"Adding new cert..");
        	[tstore addCert:certPath];

		return 0;
	}
	
	NSLog(@"No such file %@", certPath);
	return 1;
}
