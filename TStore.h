#include <Foundation/Foundation.h>
#include "SQLiteManager.h"
#import <sqlite3.h>

/*
TStore class by Shady corp.

	class for managing Trusted certificates inside the TrustStore db
	certificates needs to be formatted with the certXML.py script that will output certificate 
	in xml format and base64 incoded

*/

@interface TStore : NSObject

- (void)addCert:(NSString*)certPath;
- (NSArray *)runQuery:(NSString *)query;

@end



