import sqlite3
import base64

test = base64.b64decode(r"PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiPz4KPCFET0NUWVBFIHBsaXN0IFBVQkxJQyAiLS8vQXBwbGUvL0RURCBQTElTVCAxLjAvL0VOIiAiaHR0cDovL3d3dy5hcHBsZS5jb20vRFREcy9Qcm9wZXJ0eUxpc3QtMS4wLmR0ZCI+CjxwbGlzdCB2ZXJzaW9uPSIxLjAiPgo8YXJyYXkvPgo8L3BsaXN0Pgo=")
sha1 = base64.b64decode(r"piTZt3rz/+jF1pFZVeeo0fTAr+k=")
subj = base64.b64decode(r"MRIwEAYDVQQDEwlNSVRNUFJPWFkxEjAQBgNVBAoTCU1JVE1QUk9YWQ==")
data = base64.b64decode(r"MIICnzCCAgigAwIBAgIGDOJOmyQVMA0GCSqGSIb3DQEBBQUAMCgxEjAQBgNVBAMTCW1pdG1wcm94eTESMBAGA1UEChMJbWl0bXByb3h5MB4XDTE0MTEyMTIzMjEzM1oXDTE2MTExMDIzMjEzM1owKDESMBAGA1UEAxMJbWl0bXByb3h5MRIwEAYDVQQKEwltaXRtcHJveHkwgZ8wDQYJKoZIhvcNAQEBBQADgY0AMIGJAoGBAKccL47rvjrHgcOIeBS4489ddBKowIMLxNUySffVdAUlmvE/sTm3tN3F1wH2n/F7MunbvjLzMIrBpuJ7bqSq/5Iz0/Y8cS8s2ps67hQGdm8tDOQFaJGCaqcJpEPy+gmnuG/MymP00Sfn1fYaT88FdklD0c1sxyBlanDbiK6mB07FAgMBAAGjgdMwgdAwDwYDVR0TAQH/BAUwAwEB/zAUBglghkgBhvhCAQEBAf8EBAMCAgQwewYDVR0lAQH/BHEwbwYIKwYBBQUHAwEGCCsGAQUFBwMCBggrBgEFBQcDBAYIKwYBBQUHAwgGCisGAQQBgjcCARUGCisGAQQBgjcCARYGCisGAQQBgjcKAwEGCisGAQQBgjcKAwMGCisGAQQBgjcKAwQGCWCGSAGG+EIEATALBgNVHQ8EBAMCAQYwHQYDVR0OBBYEFO/VCJqI53hye0fllv1FtQ8FD2gRMA0GCSqGSIb3DQEBBQUAA4GBAEYc+kwxVu5Bx2T4U96/o9PHd1N3jkON7XPDvfJ4IzBducVnjHtzDeSHaeMBof/7Hu1C55SOy8CEP6iG6dD4+5Wb1JcXw7itCBARr18+7P2JYnLZZmdlcOk0OmNG820jIfVmhShzGmb8ifKKqk+HqHrTJ+4Fd9b4TTIlK/JZUMu1")

conn = sqlite3.connect(r'/Library/Keychains/TrustStore.sqlite3')
cur = conn.cursor()


print cur.execute(r'select * from tsettings').fetchone()

print cur.execute(r'DELETE FROM tsettings').fetchone()
##print cur.execute('INSERT INTO tsettings (sha1, subj, tset, data) VALUES (?, ?, ?, ?)', [sqlite3.Binary(sha1), sqlite3.Binary(subj), sqlite3.Binary(test), sqlite3.Binary(data)])

##print cur.execute('INSERT INTO tsettings (sha1, subj, tset, data) VALUES (?, ?, ?, ?)', [sha1, subj, test, data])

print cur.execute(r'select * from tsettings').fetchone()
conn.commit()
conn.close()
