#include "TStore.h"
#include "XMLReader.h"

#define TRUST_STORE_PATH @"/Library/Keychains/TrustStore.sqlite3"

@implementation TStore

- (void)addCert:(NSString *)certPath {
	NSDictionary *certData = [self xmlToDict:certPath];

	if (!certData) {
		NSLog(@"Bad XML file.");
		exit(1);
	}	

	NSArray *certValues = [self certValues:certData];
	sqlite3 *database;
	sqlite3_stmt *statement;

	if (sqlite3_open([TRUST_STORE_PATH UTF8String], &database) != SQLITE_OK) {
		NSLog(@"Error connecting to database");
	}

	NSString *querySQL = @"INSERT INTO tsettings (sha1, subj, tset, data) VALUES (?, ?, ?, ?)";
	const char *query_stmt = [querySQL UTF8String];

	if (sqlite3_prepare_v2(database, query_stmt, -1, &statement, NULL) == SQLITE_OK) {     

		for (int i=0;i<4;i++) {
			sqlite3_bind_blob(statement, i+1, [certValues[i] bytes], [certValues[i] length], 
				SQLITE_STATIC);
		}
			
	}

	if (sqlite3_step(statement) != SQLITE_DONE) {
       		NSLog(@"error: %s", sqlite3_errmsg(database));
	}

	sqlite3_finalize(statement);
	sqlite3_close(database);
}


- (NSArray *)runQuery:(NSString *)query {
	static SQLiteManager *sqliteManager = nil;
	sqliteManager = [[SQLiteManager alloc]initWithDatabaseNamed:TRUST_STORE_PATH];
	return [sqliteManager getRowsForQuery:query];
}


- (NSData *) NSDataFromBase64:(NSString *)base64String {
	NSData *decodedData = [[NSData alloc] initWithBase64EncodedString:base64String options:0];
        return decodedData;
}


- (NSArray *)certValues:(NSDictionary *)certDict {
        NSString *DTD = [[[[certDict objectForKey:@"TrustStoreRecord"] objectForKey:@"Static"]
                objectForKey:@"AppleDTD"] valueForKey:@"text"];

	NSString *fingerprint = [[[[certDict objectForKey:@"TrustStoreRecord"] objectForKey:@"Certificate"] 
		objectForKey:@"Fingerprint"] valueForKey:@"text"];

	NSString *subject = [[[[certDict objectForKey:@"TrustStoreRecord"] objectForKey:@"Certificate"]
                objectForKey:@"subject"] valueForKey:@"text"];

        NSString *data = [[[[certDict objectForKey:@"TrustStoreRecord"] objectForKey:@"Certificate"]
                objectForKey:@"data"] valueForKey:@"text"];

	return @[[self NSDataFromBase64:fingerprint], [self NSDataFromBase64:subject], [self NSDataFromBase64:DTD],
		[self NSDataFromBase64:data]];
}


- (NSDictionary *)xmlToDict:(NSString *)xmlPath {
	NSError* error = nil;

	NSString *xmlFileContents = [NSString stringWithContentsOfFile:xmlPath encoding:NSUTF8StringEncoding
		error:&error];

	if (error) {
		NSLog(@"%@", error);
	}
	
	NSDictionary *dict = [XMLReader dictionaryForXMLString:xmlFileContents options:XMLReaderOptionsProcessNamespaces error:&error];

	if (error)
		NSLog(@"%@", error);
	else
		return dict;
	
	return nil;
}


@end
